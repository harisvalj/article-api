/* eslint-disable consistent-return */
/* eslint-disable no-undef */
import { use, request } from 'chai';
import chaiHttp from 'chai-http';
import server from '../app';

use(chaiHttp);

describe('Checking endpoints', () => {
  it('Home', (done) => {
    request(server)
      .get('')
      .end((err, res) => {
        if (err) {
          return done(err);
        }
        res.should.have.status(200);
        done();
      });
  });

  it('/articles', (done) => {
    request(server)
      .get('/articles')
      .end((err, res) => {
        if (err) {
          return done(err);
        }
        res.should.have.status(200);
        res.body.should.be.a('Array');
        done();
      });
  });

  it('/nowhere', (done) => {
    request(server)
      .get('/apidontwork')
      .end((err, res) => {
        if (err) {
          return done(err);
        }
        res.should.have.status(404);
        done();
      });
  });
});
