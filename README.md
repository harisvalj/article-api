# Article API
Article API is plugin which work with Node.js and Express.js. Currently there is only one endpoint (‘/articles’) with mock-up data.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

## Prerequisites

For this project you will need:

```
-	Docker
-	Node.js
-	npm
- express.js
- eslint
- nodemon
- @babel/core
- @babel/preset-env
- @babel/register
- chai
- chai-http
- mocha
```

## Installing

A step by step series of examples that tell you how to get a development env running
To start development env just run


```
npm run dev
```

To start production env

```
npm run start 
```

On development env. it will run nodemon script which will watch every change in file and restart server.
On production env it will start node with start.js file. 

In both env it will run babel which we need for ES6 javascript features.

## Build with

* [Node.js](https://nodejs.org/en/) - Javascript engine
* [ExpressJS](https://expressjs.com/) - The web framework for Node.js

## Documentation

* [Docker](https://www.docker.com/)
* [Node.js](https://nodejs.org/en/docs/)
* [Git](https://git-scm.com/documentation)
* [mochaJS](https://mochajs.org/)
* [chaiJS](https://www.chaijs.com/)
* [babelJS](https://babeljs.io/)

