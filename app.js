import express from 'express';

import data from './mockup_data/article-data';

const app = express();

const port = 3000;

const pckg = require('./package.json');

// eslint-disable-next-line no-console
console.log(`Package name: ${pckg.name}. Version: v${pckg.version}`);

// Root location
app.get('/', (req, res) => {
  res.status(200).json({
    status: true,
  });
});

// Fetch article list from database
app.get('/articles', (request, response) => {
  // Print article in JSON format
  response.status(200).json(data.data);
});

// Export modules
export default app.listen(port);
