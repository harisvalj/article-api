const data = [
  {
    id: '586678522fe37e66355fcbd0',
    title: 'Article title 1',
    content: 'Lorem ipsum',
  },
  {
    id: '586678522fe37e66355fcbd2',
    title: 'Article title 2',
    content: 'Ipsum lorem',
  },
  {
    id: '586678522fe37e66355fcbd0',
    title: 'Article title 3',
    content: 'Lorem ipsum',
  },
  {
    id: '586678522fe37e66355fcbd2',
    title: 'Article title 4',
    content: 'Ipsum lorem',
  },
];


// Export data for articles
export default { data };
